class Menu{

    static isSehat = true;

    constructor(name,price){
        this.name = name;
        this.price = price;
    }

    menu(){
        console.log(`Nama menu ini adalah ${this.name}, harganya ${this.price} rupiah`)
    }

    pesan(){
        console.log(`Menu telah dipesan, silahkan menunggu`)
    }

}

let nasiGoreng = new Menu("Nasi Goreng" , 10000)
let mieAyam = new Menu("Mie Ayam", 15000)

Menu.prototype.enak = function(){
    console.log(`Menu ${this.name} itu enak sekali`)
}

Menu.prototype.pelanggan = function(namaPelanggan){
    console.log(`Menu ${this.name} dipesan oleh ${namaPelanggan}`)
}

console.log(nasiGoreng)
console.log(nasiGoreng instanceof Menu)
nasiGoreng.menu()
nasiGoreng.pesan()
nasiGoreng.enak()
nasiGoreng.pelanggan('Alif')



